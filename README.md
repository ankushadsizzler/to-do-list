# TO-DO-LIST #

Create a todo task list app with the following functionality:

* POST an task
* Update an task by its name
* Delete an task by its id 
* Get list of tasks(with pagination)
* Get a task by its id

The app should be based on RestFUL principles.

You may use :

* Spring Boot
* Jetty/Tomcat
* Java 8 (for date and time fields, use Java 8 Time API)
* Illustrate use of stream API if possible
* Cassandra as storage(use any Cassandra library you wish)

All the endpoints exposed must except JSON and return data in JSON format.

A task will have the following fields:

* name - String
* description - String
* id - int
* createdOn - timestamp
* status ( whether its pending or complete)

Also, make sure to insert few Task items when your Spring Boot app starts, and save them on Cassandra.
If possible, write unit test using any library that you wish.